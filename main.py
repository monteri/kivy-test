from kivy.app import App
from kivy.utils import platform

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.filechooser import FileChooserIconView
from kivy.uix.scatter import Scatter


class FileWidget(FileChooserIconView):

    def on_submit(self, selected, touch=None):
        print(self.selection)


class MyApp(App):
    _image = None
    _path = None

    def build(self):
        self._path = 'C:\\Users'

        if platform == 'android':
            from android.permissions import request_permissions, Permission
            request_permissions([Permission.WRITE_EXTERNAL_STORAGE, Permission.READ_EXTERNAL_STORAGE])
            from jnius import autoclass
            Environment = autoclass('android.os.Environment')
            self._path = Environment.getExternalStorageDirectory().getPath()

        # parent widget
        parent = BoxLayout(orientation='vertical')
        # child widgets
        bl = BoxLayout(size_hint=(1, .25))
        self.image_widget = Scatter()

        btn_open = Button(text="Open image", on_press=self.open_image, size_hint=(.4, 1))

        self.btn_file_chooser = FileWidget()
        self.btn_file_chooser.path = self._path

        bl.add_widget(btn_open)
        bl.add_widget(self.btn_file_chooser)
        parent.add_widget(bl)
        parent.add_widget(self.image_widget)
        return parent

    def open_image(self, instance):
        print(self.btn_file_chooser.selection)
        if self.btn_file_chooser.selection:
            if self._image:
                self.image_widget.canvas.clear()
            self._image = self.btn_file_chooser.selection[0]
            with self.image_widget.canvas:
                Image(source=self.btn_file_chooser.selection[0],
                      size_hint=self.image_widget.size_hint,
                      size=self.image_widget.size,
                      pos=self.image_widget.pos)


if __name__ == "__main__":
    MyApp().run()
